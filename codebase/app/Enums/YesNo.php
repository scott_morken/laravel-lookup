<?php

namespace App\Enums;

enum YesNo implements \App\Contracts\Enums\YesNo
{
    case YES;
    case NO;

    public function text(): string
    {
        return match ($this) {
            self::YES => 'Yes',
            self::NO => 'No',
        };
    }
}
