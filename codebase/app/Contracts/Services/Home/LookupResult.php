<?php

namespace App\Contracts\Services\Home;

use Smorken\Service\Contracts\Services\VO\VOResult;

/**
 * @property \App\Contracts\Services\Home\ArrayFilterResult $filterResult
 * @property \App\Contracts\Tools\History $history
 * @property string $viewName
 * @property string $message
 */
interface LookupResult extends VOResult
{
    public function addWith(string $key, mixed $value): void;

    public function addWiths(array $withs): void;

    public function getWiths(array $additional = []): array;
}
