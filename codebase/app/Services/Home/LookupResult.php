<?php

namespace App\Services\Home;

use App\Contracts\Tools\History;
use Smorken\Service\Services\VO\VOResult;

class LookupResult extends VOResult implements \App\Contracts\Services\Home\LookupResult
{
    public string $message = '';

    public string $viewName = 'index';

    protected array $withs = [];

    public function __construct(
        public \App\Contracts\Services\Home\ArrayFilterResult $filterResult,
        public History $history
    ) {
        $this->addWiths([
            'history' => $history,
            'filterResult' => $this->filterResult,
        ]);
    }

    public function addWith(string $key, mixed $value): void
    {
        $this->withs[$key] = $value;
    }

    public function addWiths(array $withs): void
    {
        foreach ($withs as $key => $value) {
            $this->addWith($key, $value);
        }
    }

    public function getWiths(array $additional = []): array
    {
        $this->addWiths($additional);
        $this->addWith('message', $this->message);

        return $this->withs;
    }
}
