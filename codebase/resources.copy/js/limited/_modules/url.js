const getUrl = (id) => {
    return document.getElementById(id).getAttribute('href');
};

module.exports.getUrl = getUrl;
