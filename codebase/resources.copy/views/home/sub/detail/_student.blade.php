<?php
/**
 * @var \Smorken\Sis\Contracts\Base\Student\Enrollment $enrollment
 */
$helper = new \App\View\Helpers\EnrollmentHelper($enrollment);
?>
<div>{{ $helper->studentName() }}
    <small class="{{ $helper->classes() }}">
        ({{ $helper->text() }})
    </small>
</div>

