@foreach (session()->all() as $key => $value)
    @if (\Illuminate\Support\Str::startsWith($key, 'flash:'))
        @include('layouts._partials._flash_message', ['key' => explode(':', substr($key, 6))[0], 'value' => $value])
    @endif
@endforeach
