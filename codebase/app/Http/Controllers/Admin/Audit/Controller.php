<?php

namespace App\Http\Controllers\Admin\Audit;

use App\Contracts\Services\Audits\Admin\IndexService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class Controller extends \Smorken\Controller\View\Controller
{
    protected string $baseView = 'admin.audit';

    public function __construct(protected IndexService $indexService)
    {
        parent::__construct();
    }

    public function index(Request $request): View
    {
        $result = $this->indexService->getByRequest($request);

        return $this->makeView($this->getViewName('index'))
            ->with('models', $result->models)
            ->with('filter', $result->filter);
    }
}
