<?php
/**
 * @var \App\Contracts\Services\Home\ArrayFilterResult $filterResult
 * @var string $message
 */
?>
@extends('layouts.app')
@section('content')
    <div class="content">
        @if ($message)
            <div class="alert alert-info">{{ $message }}</div>
        @endif
        @include('home._search_form')
    </div>
@endsection
