<?php
$sub_menu = \Smorken\Menu\Facades\Menu::getSubMenus();
$next = null;
?>
@if ($sub_menu)
    <nav class="sub-menu my-1">
        <ul class="nav nav-pills">
            @foreach ($sub_menu as $menu)
                <?php $active = Menu::isActiveChain($controller ?? null, $menu); ?>
                @if ($active && count($menu->children))
                    <?php $next = $menu->children; ?>
                @endif
                @include('layouts.menus._menu_item')
            @endforeach
        </ul>
    </nav>
@endif
@if ($next)
    @include('layouts.menus.submenu', ['sub_menu' => $next])
@endif
