<?php
/**
 * @var \Smorken\Lookup\Contracts\Models\Klass $model
 */
?>
<div class="card">
    <div class="card-body">
        <h5 class="card-title">{{ $model->className }} {{ $model->classNumber }}</h5>
        <div class="card-text">
            @include('home.sub.detail._klass', ['klass' => $model])
        </div>
    </div>
</div>
