<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 5/24/18
 * Time: 7:35 AM
 */

namespace App\Contracts\Models;

use Smorken\Model\Contracts\Model;

/**
 * Interface Audit
 *
 *
 * @property int $id
 * @property string $lookup_id
 * @property \Smorken\Lookup\Constants\LookupType $lookup_type
 * @property int $user_id
 */
interface Audit extends Model
{
}
