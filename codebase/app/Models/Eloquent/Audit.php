<?php

namespace App\Models\Eloquent;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Lookup\Constants\LookupType;

class Audit extends Base implements \App\Contracts\Models\Audit
{
    protected $casts = [
        'lookup_type' => LookupType::class,
    ];

    protected $fillable = ['lookup_id', 'lookup_type', 'user_id'];

    public function scopeCreatedAfter(Builder $query, string|Carbon $date): Builder
    {
        return $query->whereDate('created_at', '>=', $date);
    }

    public function scopeCreatedBefore(Builder $query, string|Carbon $date): Builder
    {
        return $query->whereDate('created_at', '<=', $date);
    }

    public function scopeDefaultOrder(Builder $query): Builder
    {
        return $query->orderBy('created_at', 'desc');
    }

    public function scopeDefaultWiths(Builder $query): Builder
    {
        return $query;
    }

    public function scopeLookupIdIs(Builder $query, string $id): Builder
    {
        return $query->where('lookup_id', '=', $id);
    }

    public function scopeLookupTypeIs(
        Builder $query,
        LookupType $type
    ): Builder {
        return $query->where('lookup_type', '=', $type);
    }

    public function scopeUserIdIs(Builder $query, int $userId): Builder
    {
        return $query->where('user_id', '=', $userId);
    }
}
