<?php

namespace App\Contracts\Services\Home;

use App\Contracts\Tools\History;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\Request;
use Smorken\Lookup\Constants\LookupType;
use Smorken\Lookup\Factory;
use Smorken\Service\Contracts\Services\BaseService;

interface LookupService extends BaseService
{
    public function getArrayFilterService(): ArrayFilterService;

    public function getEventDispatcher(): Dispatcher;

    public function getHistory(): History;

    public function getLookup(): Factory;

    public function lookup(Request $request): LookupResult;

    public function view(Request $request, LookupType|string $type, string $id): LookupResult;
}
