<?php
/**
 * @var \Smorken\Lookup\Contracts\Models\Person $model
 * @var \Smorken\Hrms\Contracts\Models\Job $job
 */
?>
<div class="card">
    <div class="card-body">
        <h5 class="card-title person-name">
            {{ $model->fullName() }}
            @include('home.sub._toggle_mask')
        </h5>
        <div class="card-text">
            <div class="person-id can-blur blur">{{ $model->id }}</div>
            <div class="person-altid can-blur blur">{{ $model->altId }}</div>
            <div class="person-email">
                <a href="mailto:{{ $model->email }}" title="Email {{ $model->email }}"
                   class="can-blur blur text-underline-hover">
                    {{ $model->email }}
                </a>
            </div>
            <div class="person-phone">
                @if ($model->phone)
                    <a href="tel:{{ preg_replace('/[^0-9\+]+/', '', $model->phone) }}"
                       title="Call {{ $model->phone }}" class="can-blur blur text-underline-hover"
                    >{{ $model->phone }}</a>
                @else
                    No phone
                @endif
            </div>
            <hr/>
            <div class="row">
                <?php $yesNoHelper = new \App\View\Helpers\YesNoHelper((bool) $model->isEmployee()); ?>
                <div class="col">
                    Employee: <i class="{{ $yesNoHelper->classes() }}">
                        <span class="visually-hidden">{{ $yesNoHelper->text() }}</span>
                    </i>
                </div>
                <?php $yesNoHelper = new \App\View\Helpers\YesNoHelper((bool) $model->isResidential()); ?>
                <div class="col">
                    Residential: <i class="{{ $yesNoHelper->classes() }}">
                        <span class="visually-hidden">{{ $yesNoHelper->text() }}</span>
                    </i>
                </div>
                <?php $yesNoHelper = new \App\View\Helpers\YesNoHelper((bool) $model->isAdjunct()); ?>
                <div class="col">
                    Adjunct: <i class="{{ $yesNoHelper->classes() }}">
                        <span class="visually-hidden">{{ $yesNoHelper->text() }}</span>
                    </i>
                </div>
            </div>
            @if ($model->isEmployee())
                @foreach ($model->jobs as $job)
                    <div>
                        {{ $job->positionDescription }} @ {{ $job->locationDescription }}
                        @if ($job->isPrimary())
                            *
                        @endif
                    </div>
                @endforeach
                <small class="text-muted">* indicates primary job</small>
            @endif
            @include('home.sub._blur_help')
        </div>
    </div>
</div>
