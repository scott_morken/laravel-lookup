<?php

namespace App\Traits;

use Illuminate\Container\Container;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Foundation\Application;

trait App
{
    /**
     * @var Container|Application|null
     */
    protected \Illuminate\Contracts\Container\Container|Application|null $app = null;

    /**
     * @var array<string,mixed>
     */
    protected array $providers = [];

    /**
     * @return Container|Application
     */
    public function getApp(): \Illuminate\Contracts\Container\Container|Application
    {
        if (is_null($this->app)) {
            $this->app = \Illuminate\Support\Facades\App::getFacadeRoot();
        }

        return $this->app;
    }

    public function setApp(Application $app): void
    {
        $this->app = $app;
    }

    protected function make(string $contract, array $params = []): mixed
    {
        if (! isset($this->providers[$contract])) {
            try {
                $this->providers[$contract] = $this->getApp()->make($contract, $params);
            } catch (BindingResolutionException) {
                $trimmed = trim($contract, '\\');
                $this->providers[$contract] = $this->getApp()->make($trimmed, $params);
            }
        }

        return $this->providers[$contract];
    }
}
