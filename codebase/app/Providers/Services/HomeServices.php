<?php

namespace App\Providers\Services;

use App\Contracts\Services\Home\LookupService;
use App\Contracts\Tools\History;
use App\Services\Home\ArrayFilterService;
use Illuminate\Contracts\Events\Dispatcher;
use Smorken\Lookup\Factory;
use Smorken\Service\Invokables\Invokable;

class HomeServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(LookupService::class, function ($app) {
            $filterService = new ArrayFilterService();

            return new \App\Services\Home\LookupService(
                $app[Factory::class],
                $app[History::class],
                $app[Dispatcher::class],
                [
                    \App\Contracts\Services\Home\ArrayFilterService::class => $filterService,
                ]
            );
        });
    }
}
