<?php

namespace App\Http\Controllers;

use App\Contracts\Services\Home\LookupResult;
use App\Contracts\Services\Home\LookupService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class HomeController extends \Smorken\Controller\View\Controller
{
    protected string $baseView = 'home';

    public function __construct(protected LookupService $service)
    {
        parent::__construct();
    }

    public function index(Request $request): View
    {
        $result = $this->service->lookup($request);

        return $this->fromResult($result);
    }

    public function view(Request $request, string $type, string $id): View
    {
        $result = $this->service->view($request, $type, $id);

        return $this->fromResult($result);
    }

    protected function fromResult(LookupResult $result, array $additionalWiths = []): View
    {
        $view = $this->makeView($this->getViewName($result->viewName));
        foreach ($result->getWiths($additionalWiths) as $key => $value) {
            $view->with($key, $value);
        }

        return $view;
    }
}
