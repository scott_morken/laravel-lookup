<?php
/**
 * @var \Smorken\Lookup\Contracts\Models\Person $model
 */
?>
<div class="row select-person">
    <div class="col">{{ $model->fullName() }}</div>
    <div class="col can-blur blur">{{ $model->id }}</div>
    <div class="col can-blur blur">{{ $model->altId }}</div>
    <div class="col">
        <div class="row">
            <?php $yesNoHelper = new \App\View\Helpers\YesNoHelper((bool) $model->isEmployee()); ?>
            <div class="col">
                Employee: <i class="{{ $yesNoHelper->classes() }}">
                    <span class="visually-hidden">{{ $yesNoHelper->text() }}</span>
                </i>
            </div>
            <?php $yesNoHelper = new \App\View\Helpers\YesNoHelper((bool) $model->isFaculty()); ?>
            <div class="col">
                Faculty: <i class="{{ $yesNoHelper->classes() }}">
                    <span class="visually-hidden">{{ $yesNoHelper->text() }}</span>
                </i>
            </div>
        </div>
    </div>
</div>
