<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 5/24/18
 * Time: 7:36 AM
 */

namespace App\Contracts\Storage;

use Smorken\Lookup\Constants\LookupType;
use Smorken\Storage\Contracts\Base;

interface Audit extends Base
{
    public function store(
        string $lookupId,
        LookupType $lookupType,
        int $userId
    ): bool;
}
