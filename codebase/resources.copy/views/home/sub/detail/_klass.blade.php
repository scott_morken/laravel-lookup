<?php
/**
 * @var \Smorken\Lookup\Contracts\Models\Klass $klass
 */
?>
<div class="class-detail">
    <h6 class="class-name">{{ $klass->className }} {{ $klass->classNumber }}
        [{{ $klass->componentDescription }}] <small>{{ $klass->units ?: 'N/A' }} units</small></h6>
    <div class="class-descr">{{ $klass->description }}</div>
    @if ($klass->cancelDate)
        <div class="text-danger">Cancelled</div>
    @endif
    <div class="class-term">{{ $klass->term ? $klass->term->description : $klass->termId }}</div>
    <div class="class-dates">{{ $klass->startDate->toFormattedDateString() }}
        - {{ $klass->endDate->toFormattedDateString() }}</div>
    <div>
        <span class="class-status">{{ $klass->statusDescription }}</span> &middot;
        <span class="class-enrollstatus">{{ $klass->enrollmentStatusDescription }}</span> &middot;
        <span class="class-dayeve">{{ $klass->dayEveningDescription }}</span> &middot;
        <span class="class-instrmode">{{ $klass->instructionModeDescription }}</span>
    </div>
</div>
