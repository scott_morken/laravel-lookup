<?php

namespace Tests\App\Browser;

use Carbon\Carbon;
use Smorken\Hrms\Contracts\Models\Hcm;
use Smorken\Hrms\Models\Enums\FacultyIndicators;
use Smorken\Sis\Contracts\Base\Klass\Klass;
use Smorken\Sis\Contracts\Base\Person\Person;
use Smorken\Sis\Contracts\Base\Student\Enrollment;
use Smorken\Sis\Contracts\Base\Term;
use Smorken\Sis\Db\Incremental\Klass\ClassInstructor;
use Smorken\Sis\Enums\Student\EnrollmentStatuses;

trait CreateData
{
    protected array $data = [
        'term' => null,
        'people' => [],
        'employees' => [],
        'classes' => [],
        'classInstructors' => [],
        'enrollments' => [],
    ];

    protected int $increment = 0;

    protected function addInstructorForKlass(Klass $klass, int $instructorId): \Smorken\Sis\Contracts\Base\Klass\ClassInstructor
    {
        return ClassInstructor::factory()->create([
            'CLASS_COURSE_ID' => $klass->CLASS_COURSE_ID,
            'CLASS_COURSE_OFFER_NBR' => $klass->CLASS_COURSE_OFFER_NBR,
            'CLASS_TERM_CD' => $klass->CLASS_TERM_CD,
            'CLASS_SESSION_CD' => $klass->CLASS_SESSION_CD,
            'CLASS_SECTION' => $klass->CLASS_SECTION,
            'CLASSM_INSTRUCTOR_EMPLID' => $instructorId,
        ]);
    }

    protected function createClass(Term $term, array $attributes = []): Klass
    {
        $classSubject = 'ABC';
        $catalogNumber = 100 + $this->increment;
        $attributes = array_merge([
            'CLASS_SECTION' => (10 + $this->increment),
            'CLASS_SUBJECT_CD' => $classSubject,
            'CLASS_CATALOG_NBR' => $catalogNumber,
            'CLASS_TERM_CD' => $term->id,
            'CLASS_CLASS_NAME' => $classSubject.$catalogNumber,
            'CLASS_CLASS_NBR' => (10000 + $this->increment),
            'CLASS_DESCR' => $classSubject.$catalogNumber.' Class',
            'CLASS_INSTITUTION_CD' => 'COLL01',
            'CLASS_START_DATE' => Carbon::now()->subDay(),
            'CLASS_END_DATE' => Carbon::now()->addMonth(),
        ], $attributes);
        $this->increment++;

        return \Smorken\Sis\Db\Incremental\Klass\Klass::factory()->create($attributes);
    }

    protected function createHcmPerson(array $attributes = []): Hcm
    {
        if (! $attributes) {
            $id = 100 + $this->increment;
            $oprId = 'ABC'.(1000000 + $id);
            $firstName = 'First'.$id;
            $lastName = 'Last'.$id;
            $this->increment++;
            $attributes = [
                'MC_HCM_EMPLID' => $id + 10000000,
                'MC_MEID' => $oprId,
                'CAMPUS_ID' => $id,
                'LAST_NAME' => $lastName,
                'FIRST_NAME' => $firstName,
            ];
        }

        return \Smorken\Hrms\Models\Eloquent\Hcm::factory()->create($attributes);
    }

    protected function createHcmPersonFromPerson(Person $person, array $attributes = []): Hcm
    {
        return $this->createHcmPerson(array_merge([
            'MC_HCM_EMPLID' => $person->id + 10000000,
            'MC_MEID' => $person->altId,
            'CAMPUS_ID' => $person->id,
            'LAST_NAME' => $person->lastName,
            'FIRST_NAME' => $person->firstName,
        ], $attributes));
    }

    protected function createPerson(int $id, array $attributes = []): Person
    {
        $oprId = 'ABC'.(1000000 + $id);
        $firstName = 'First'.$id;
        $lastName = 'Last'.$id;

        return \Smorken\Sis\Db\Incremental\Person\Person::factory()->create(array_merge([
            'PERS_EMPLID' => $id,
            'PERS_OPRID' => $oprId,
            'PERS_PRIMARY_FIRST_NAME' => $firstName,
            'PERS_PRIMARY_LAST_NAME' => $lastName,
        ], $attributes));
    }

    protected function createTerm(int $termId = 1234, array $attributes = []): Term
    {
        return \Smorken\Sis\Db\Core\Term::factory()->create(array_merge([
            'STRM' => $termId,
            'DESCR' => 'Term '.$termId,
            'TERM_BEGIN_DT' => Carbon::now()->subWeek(),
            'TERM_END_DT' => Carbon::now()->addMonth(),
            'INSTITUTION' => 'COLL01',
        ], $attributes));
    }

    protected function enrollInClass(
        Klass $klass,
        int $studentId,
        string $status = EnrollmentStatuses::ENROLLED
    ): Enrollment {
        return \Smorken\Sis\Db\Incremental\Student\Enrollment::factory()->create([
            'ENRL_EMPLID' => $studentId,
            'ENRL_STATUS_REASON_CD' => $status,
            'ENRL_TERM_CD' => $klass->CLASS_TERM_CD,
            'ENRL_CLASS_NBR' => $klass->CLASS_CLASS_NBR,
        ]);
    }

    protected function seedSis(): void
    {
        $this->data['people'][] = $this->createPerson(2);
        $this->data['people'][] = $this->createPerson(3);
        $this->data['people'][] = $this->createPerson(4);
        $this->data['people'][] = $this->createPerson(22);
        $this->data['people'][] = $this->createPerson(33);
        $this->data['people'][] = $this->createPerson(44);
        foreach (array_slice($this->data['people'], 0, 2) as $employee) {
            $this->data['employees'][] = $this->createHcmPersonFromPerson($employee, [
                'MC_FACULTY_IND' => FacultyIndicators::CURRENT,
                'JOBCODE' => 6301,
            ]);
        }
        $this->data['employees'][] = $this->createHcmPersonFromPerson($this->data['people'][2], [
            'MC_FACULTY_IND' => FacultyIndicators::NEVER,
            'JOBCODE' => 9999,
        ]);
        $this->data['employees'][] = $this->createHcmPerson([
            'MC_FACULTY_IND' => FacultyIndicators::NEVER,
            'JOBCODE' => 9999,
        ]);
        $this->data['term'] = $term = $this->createTerm();
        $this->data['classes'][] = $klass = $this->createClass($term);
        $this->data['classInstructors'][] = $this->addInstructorForKlass($klass, 2);
        $this->data['enrollments'][] = $this->enrollInClass($klass, 22);
        $this->data['enrollments'][] = $this->enrollInClass($klass, 33);
        $this->data['classes'][] = $klass2 = $this->createClass($term);
        $this->data['classInstructors'][] = $this->addInstructorForKlass($klass2, 3);
        $this->data['enrollments'][] = $this->enrollInClass($klass2, 22);
        $this->data['enrollments'][] = $this->enrollInClass($klass2, 44);
    }
}
