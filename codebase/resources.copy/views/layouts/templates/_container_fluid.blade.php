<div class="container-fluid">
    @includeIf('layouts.menus.submenu')
</div>
<main class="py-4 container-fluid">
    @yield('content')
</main>
<footer class="py-4 container-fluid">
    <div class="text-center">
        <div class="text-muted"><small>&copy; {{ date('Y') }} Phoenix College</small>
            @if (\Illuminate\Support\Facades\Session::has('impersonating'))
                <span class="text-danger">&middot; Impersonating [{{ \Illuminate\Support\Facades\Session::get('impersonating') }}]</span>
            @endif
            <div>
                <a href="https://www.maricopa.edu" target="_blank" title="Maricopa Community Colleges" rel="noopener">
                    <img src="{{ asset('images/footer.png') }}" alt="A Maricopa Community College"
                         height="30">
                </a>
            </div>
        </div>
    </div>
    @yield('footer')
</footer>
@include('layouts._partials._modal')
