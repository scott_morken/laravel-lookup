<?php

namespace App\Contracts\Enums;

interface YesNo
{
    public function text(): string;
}
