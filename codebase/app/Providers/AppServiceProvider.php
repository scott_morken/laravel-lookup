<?php

namespace App\Providers;

use App\Contracts\Tools\History;
use App\Events\Audit;
use App\Listeners\AuditLookup;
use App\Tools\History\Session;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Http\Middleware\TrustProxies;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;
use Smorken\SocialAuth\SocialiteProviders\AzureLatestExtendSocialite;
use SocialiteProviders\Manager\SocialiteWasCalled;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $this->registerSocialAuthEvents();
        $this->registerEventListeners();
        $this->setTrustedProxies();
    }

    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->bindHistory();
    }

    protected function bindHistory(): void
    {
        $this->app->bind(History::class, function ($app) {
            /** @var UrlGenerator $urlGenerator */
            $urlGenerator = $app[UrlGenerator::class];

            return new Session(
                $urlGenerator->route('home'),
                $app['session'],
                $urlGenerator
            );
        });
    }

    protected function registerEventListeners(): void
    {
        Event::listen(Audit::class, AuditLookup::class);
    }

    protected function registerSocialAuthEvents(): void
    {
        Event::listen(SocialiteWasCalled::class, AzureLatestExtendSocialite::class);
        //Event::listen(SocialiteWasCalled::class, FakeExtendSocialite::class);
    }

    protected function setTrustedProxies(): void
    {
        TrustProxies::at('*');
    }
}
