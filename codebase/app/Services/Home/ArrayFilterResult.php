<?php

namespace App\Services\Home;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Smorken\Service\Services\VO\VOResult;

class ArrayFilterResult extends VOResult implements \App\Contracts\Services\Home\ArrayFilterResult
{
    public array $filter = [];

    protected array $keys = [
        'filter.person.firstName',
        'filter.person.lastName',
        'filter.person.id',
        'filter.class.norn',
        'filter.class.termId',
    ];

    public function __construct(protected Request $request)
    {
        foreach ($this->keys as $key) {
            $this->set($key, $this->request->input($key));
        }
    }

    public function get(string $key, mixed $default = null): mixed
    {
        return Arr::get($this->filter, $key, $default);
    }

    public function set(string $key, mixed $value): void
    {
        Arr::set($this->filter, $key, $value);
    }

    public function valueOf(string $key): mixed
    {
        return $this->request->old($key, $this->get($key));
    }
}
