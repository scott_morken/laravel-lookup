<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['auth', 'can:role-manage'])
    ->group(function () {
        Route::get('/', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');
        Route::get('/view/{type}/{id}', [\App\Http\Controllers\HomeController::class, 'view']);
    });

Route::middleware(['auth', 'can:role-admin'])
    ->prefix('admin')
    ->group(function () {
        Route::prefix('audit')
            ->group(function () {
                Route::get('/', [\App\Http\Controllers\Admin\Audit\Controller::class, 'index']);
            });
    });
