<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('audits');
    }

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create(
            'audits',
            function (Blueprint $t) {
                $t->increments('id');
                $t->string('lookup_id', 32);
                $t->string('lookup_type', 16);
                $t->integer('user_id')
                    ->unsigned();
                $t->timestamps();

                $t->index('lookup_type', 'aud_lookup_type_ndx');
                $t->index('lookup_id', 'aud_lookup_id_ndx');
                $t->index('user_id', 'aud_user_id_ndx');
            }
        );
    }
};
