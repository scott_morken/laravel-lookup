<?php
/**
 * @var LookupType $type
 */
use Smorken\Lookup\Constants\LookupType;

?>
@extends('layouts.app')
@section('content')
    @include('home._breadcrumbs')
    <div class="row">
        <div class="col-md">
            @if ($type === LookupType::PERSON)
                @include('home.sub.detail._person_card')
            @else
                @include('home.sub.detail._klass_card')
            @endif
        </div>
        <div class="col-md">
            @if ($type === LookupType::PERSON)
                @include('home.sub.related._klasses_card')
            @else
                @include('home.sub.related._people_card')
            @endif
        </div>
    </div>
    @include('home.sub._disclaimer')
@endsection
@push('add_to_end')
    <script src="{{ asset('js/limited/home.mask.js') }}"></script>
@endpush
