<?php

namespace App\Services\Home;

use App\Contracts\Services\Home\ArrayFilterResult;
use Illuminate\Http\Request;
use Smorken\Service\Services\BaseService;

class ArrayFilterService extends BaseService implements \App\Contracts\Services\Home\ArrayFilterService
{
    public function getFromRequest(Request $request): ArrayFilterResult
    {
        return new \App\Services\Home\ArrayFilterResult($request);
    }
}
