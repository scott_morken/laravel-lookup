<?php

namespace App\Tools\History;

use App\Contracts\Tools\History;
use Illuminate\Contracts\Routing\UrlGenerator;

abstract class Base implements History
{
    protected mixed $backend;

    protected array $history = [
        'home' => null,
        'search' => null,
        'trail' => [],
    ];

    protected bool $initialized = false;

    public function __construct(
        string $home,
        mixed $backend,
        protected UrlGenerator $urlGenerator,
        protected int $trailCount = 3,
        protected string $key = 'lookup_history'
    ) {
        $this->setBackend($backend);
        $this->history['home'] = $home;
    }

    public function get(): array
    {
        $this->initFromBackend();

        return $this->history;
    }

    public function getBackend(): mixed
    {
        return $this->backend;
    }

    public function setBackend(mixed $backend): void
    {
        $this->backend = $backend;
    }

    public function getByKey(string $key): mixed
    {
        return $this->get()[$key] ?? null;
    }

    public function getOrdered(): array
    {
        $ordered = [];
        $hist = $this->get();
        if ($hist['home']) {
            $ordered['Home'] = $hist['home'];
            if ($hist['search']) {
                $ordered['Search'] = $hist['search'];
            }
            foreach ($hist['trail'] as $i => $data) {
                $key = $data['name'] ?: sprintf('Trail #%d', $i + 1);
                $ordered[$key] = $data['value'];
            }
        }

        return $ordered;
    }

    public function getUrlGenerator(): UrlGenerator
    {
        return $this->urlGenerator;
    }

    public function set(string $key, mixed $value, ?string $name = null): void
    {
        $this->initFromBackend();
        if ($key === 'trail') {
            $this->handleTrail($value, $name);
        } else {
            if ($key === 'search') {
                $this->history['trail'] = [];
            }
            $this->history[$key] = $value;
        }
        $this->backend->put($this->key, $this->history);
    }

    protected function handleTrail($value, $name)
    {
        if ($this->trailCount > 1) {
            if (! in_array($value, $this->history['trail'])) {
                $keep = ($this->trailCount - 1) * -1;
                $trail = array_slice($this->history['trail'], $keep);
            }
        }
        $trail[] = ['value' => $value, 'name' => $name];
        $this->history['trail'] = $trail;
    }

    protected function initFromBackend(): void
    {
        if (! $this->initialized && $this->getBackend()->exists($this->key)) {
            $orig = $this->getBackend()->get($this->key);
            foreach ($orig as $k => $v) {
                $this->history[$k] = $v;
            }
        }
        $this->initialized = true;
    }
}
