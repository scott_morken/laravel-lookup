<?php

namespace Tests\App\Browser\Tests;

use Facebook\WebDriver\WebDriverBy;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Smorken\Auth\Models\Eloquent\User;
use Smorken\Roles\Models\Eloquent\RoleUser;
use Tests\App\Browser\CreateData;
use Tests\App\DuskTestCase;

class HomeTest extends DuskTestCase
{
    use CreateData, DatabaseMigrations;

    public function testHomeRouteRequiresAuthentication(): void
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->assertPathIs('/login');
        });
    }

    public function testManagerUserIsAllowed(): void
    {
        $user = User::factory()->create([
            'id' => 2,
        ]);
        RoleUser::factory()->create(['user_id' => 2, 'role_id' => 2]);
        $this->browse(function (Browser $browser) use ($user) {
            $this->seedSis();
            $browser->loginAs($user)
                ->visit('/')
                ->assertDontSee('unauthorized')
                ->assertSee('Person Search')
                ->logout();
        });
    }

    public function testNonManagerUserIsUnauthorized(): void
    {
        $user = User::factory()->create([
            'id' => 99,
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/')
                ->assertSee('unauthorized')
                ->logout();
        });
    }

    public function testPersonExistsOnlyInHcm(): void
    {
        $user = User::factory()->create([
            'id' => 1,
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $this->seedSis();
            /** @var \Smorken\Lookup\Contracts\Models\Hcm $employee */
            $employee = $this->data['employees'][3];
            $browser->loginAs($user)
                ->visit('/')
                ->type('@search-last-name', $employee->lastName)
                ->press('@search-button')
                ->assertSourceHas('Employee: <i class="text-success bi bi-check')
                ->assertSee('Not enrolled')
                ->logout();
        });
    }

    public function testSearchClass(): void
    {
        $user = User::factory()->create([
            'id' => 1,
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $this->seedSis();
            /** @var \Smorken\Lookup\Contracts\Models\Klass $klass */
            $klass = $this->data['classes'][0];
            $browser->loginAs($user)
                ->visit('/')
                ->select('@search-class-term', $klass->termId)
                ->type('@search-class-norn', $klass->subject.$klass->catalogNumber)
                ->press('@search-button')
                ->assertSee($klass->className.' '.$klass->classNumber)
                ->logout();
        });
    }

    public function testSearchFollowChain(): void
    {
        $user = User::factory()->create([
            'id' => 1,
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $this->seedSis();
            /** @var \Smorken\Lookup\Contracts\Models\Enrollment $enrollment */
            $enrollment = $this->data['enrollments'][0];
            $browser->loginAs($user)
                ->visit('/')
                ->type('@search-person-id', $enrollment->studentId)
                ->press('@search-button')
                ->assertSourceHas('<div class="person-id can-blur blur">'.$enrollment->studentId.'</div>');
            $elements = $browser->driver->findElements(WebDriverBy::className('attends-klass'));
            $this->assertGreaterThanOrEqual(1, count($elements));
            $bcelements = $browser->driver->findElements(WebDriverBy::className('breadcrumb-item'));
            $this->assertCount(3, $bcelements); //home / search / person
            $browser->visit($elements[0]->getAttribute('href'))
                ->assertSourceHas('<h6 class="class-name">ABC')
                ->assertSourceHas('<h4 class="card-title">Students</h4>')
                ->assertSourceHas('title="View student #'.$enrollment->studentId);
            $bcelements = $browser->driver->findElements(WebDriverBy::className('breadcrumb-item'));
            $this->assertCount(4, $bcelements); //home / search / person / class
            $browser->logout();
        });
    }

    public function testSearchPersonMultipleResults(): void
    {
        $user = User::factory()->create([
            'id' => 1,
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $p1 = $this->createPerson(10, [
                'PERS_PRIMARY_LAST_NAME' => 'Foobar',
            ]);
            $p2 = $this->createPerson(11, [
                'PERS_PRIMARY_LAST_NAME' => 'Foobar',
            ]);
            $p3 = $this->createPerson(12, [
                'PERS_PRIMARY_LAST_NAME' => 'Foobar',
            ]);
            $this->seedSis();
            $browser->loginAs($user)
                ->visit('/')
                ->type('@search-last-name', 'Foo')
                ->press('@search-button');
            $elements = $browser->driver->findElements(WebDriverBy::className('select-result'));
            $this->assertCount(3, $elements);
            $bcelements = $browser->driver->findElements(WebDriverBy::className('breadcrumb-item'));
            $this->assertCount(2, $bcelements);
            $browser->visit($elements[0]->getAttribute('href'))
                ->assertSee('Foobar')
                ->assertSee('Enrollments');
            $bcelements = $browser->driver->findElements(WebDriverBy::className('breadcrumb-item'));
            $this->assertCount(3, $bcelements);
            $browser->logout();
        });
    }
}
