```shell
$ ./config_stack deploy
$ ./secrets_stack deploy
$ #first run
$ npm run static-config #if you want to use the local config over the aws param
$ ./pipeline_stack
```

Force new deployment of cluster services
```shell
$ aws ecs update-service --force-new-deployment --cluster pcc-sdlc-app-cluster --service pcc-sdlc-app-service --profile AWSACCOUNTID --region us-west-2
```

List listener rules (get next priority)
```shell
$ aws elbv2 describe-rules --listener-arn arn:aws:elasticloadbalancing:us-west-2:... --profile PROFILE --region us-west-2
```
