<?php
/**
 * @var \Smorken\Lookup\Contracts\Models\Klass $model
 */
?>
<div class="card">
    <div class="card-body">
        @include('home.sub.related._meetings_card')
        @include('home.sub.related._students_card')
    </div>
</div>
