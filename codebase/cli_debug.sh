#!/usr/bin/env bash
export XDEBUG_MODE=debug XDEBUG_SESSION=1 PHP_IDE_CONFIG="serverName=app.docker" &&
php "$@"
