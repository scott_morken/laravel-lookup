<?php

namespace App\View\Composers;

use Illuminate\Contracts\View\View;
use Smorken\Lookup\Factories\TermRepositoryFactory;

class Terms
{
    public function __construct(
        protected TermRepositoryFactory $factory,
    ) {
    }

    public function compose(View $view): void
    {
        $terms = $this->factory->all();
        $activeTerm = $this->factory->findActiveOrNext();
        $view->with('terms', $terms)
            ->with('activeTerm', $activeTerm);
    }
}
