<?php

return new class extends \Illuminate\Database\Migrations\Migration
{
    use \Tests\App\Traits\Table;

    protected $connection = null;

    protected array $tables = [
        \Smorken\Hrms\Models\Eloquent\Hcm::class,
        \Smorken\Sis\Db\Incremental\Student\Enrollment::class,
        \Smorken\Sis\Db\Incremental\Klass\Klass::class,
        \Smorken\Sis\Db\Incremental\Klass\Meeting::class,
        \Smorken\Sis\Db\Core\Term::class,
        \Smorken\Sis\Db\Incremental\Person\Person::class,
        \Smorken\Sis\Db\Incremental\Klass\ClassInstructor::class,
    ];

    public function down(): void
    {
        foreach ($this->tables as $t) {
            $this->deleteTableFromModelClass($t);
        }
    }

    public function up(): void
    {
        foreach ($this->tables as $t) {
            $this->createTableFromModelClass($t);
        }
    }
};
