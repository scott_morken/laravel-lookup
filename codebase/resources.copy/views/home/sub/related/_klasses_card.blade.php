<?php
/**
 * @var \Smorken\Lookup\Contracts\Models\Person $model
 */
?>
<div class="card">
    <div class="card-body">
        @include('home.sub.related._instructs')
        @include('home.sub.related._attends')
    </div>
</div>
