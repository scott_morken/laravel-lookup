<?php

return [
    'guest' => [],
    'auth' => [],

    'role-manage' => [],
    'role-admin' => [
        [
            'name' => 'Audits',
            'action' => [\App\Http\Controllers\Admin\Audit\Controller::class, 'index'],
            'children' => [],
        ],
        [
            'name' => 'Users',
            'action' => [\Smorken\SocialAuth\Http\Controllers\Admin\Controller::class, 'index'],
            'children' => [],
        ],
    ],
];
