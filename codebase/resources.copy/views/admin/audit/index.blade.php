<?php
/**
 * @var \Illuminate\Support\Collection $models
 * @var \App\Contracts\Models\Audit $model
 */
?>
@extends('layouts.app')
@section('content')
    <h4>Audit</h4>
    @include('admin.audit._filter_form')
    <table class="table table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>User ID</th>
            <th>Lookup ID</th>
            <th>Lookup Type</th>
            <th>Date</th>
        </tr>
        </thead>
        <tbody>
        @if ($models && count($models))
            @foreach ($models as $model)
                <tr>
                    <td>{{ $model->id }}</td>
                    <td>{{ $model->user_id }}</td>
                    <td>{{ $model->lookup_id }}</td>
                    <td>{{ $model->lookup_type?->text() }}</td>
                    <td>{{ $model->created_at }}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="5">No records found.</td>
            </tr>
        @endif
        </tbody>
    </table>
    {{ $models->appends($filter->except(['page']))->links() }}
@endsection
