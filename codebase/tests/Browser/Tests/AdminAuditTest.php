<?php

namespace Tests\App\Browser\Tests;

use Facebook\WebDriver\WebDriverBy;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Smorken\Auth\Models\Eloquent\User;
use Tests\App\Browser\CreateData;
use Tests\App\DuskTestCase;

class AdminAuditTest extends DuskTestCase
{
    use CreateData, DatabaseMigrations;

    public function testNonAdminUserIsUnauthorized(): void
    {
        $user = User::factory()->create([
            'id' => 99,
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('admin/audit')
                ->assertSee('unauthorized')
                ->logout();
        });
    }

    public function testSearchChainCreatesAuditTrail(): void
    {
        $user = User::factory()->create([
            'id' => 1,
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $this->seedSis();
            /** @var \Smorken\Lookup\Contracts\Models\Enrollment $enrollment */
            $enrollment = $this->data['enrollments'][0];
            $browser->loginAs($user)
                ->visit('/')
                ->type('@search-person-id', $enrollment->studentId)
                ->press('@search-button')
                ->assertPathIs('/')
                ->assertSourceHas('<div class="person-id can-blur blur">'.$enrollment->studentId.'</div>');
            $elements = $browser->driver->findElements(WebDriverBy::className('attends-klass'));
            $browser->visit($elements[0]->getAttribute('href'))
                ->assertSourceHas('<h6 class="class-name">ABC')
                ->assertSourceHas('<h4 class="card-title">Students</h4>')
                ->assertSourceHas('title="View student #'.$enrollment->studentId)
                ->visit('/admin/audit')
                ->assertSourceHas('<td>'.$enrollment->studentId.'</td>')
                ->assertSourceHas('<td>Person</td>')
                ->assertSourceHas('<td>Class</td>')
                ->logout();
        });
    }
}
