<?php
/**
 * @var \Smorken\Lookup\Contracts\Models\Klass $model
 */
?>
<div class="row select-klass">
    <div class="col">
        {{ $model->getShortName() }} {{ $model->classNumber }}
    </div>
    <div class="col">
        {{ $model->term ? $model->term->description : $model->termId }}
    </div>
    <div class="col">
        {{ $model->componentDescription }}
    </div>
</div>
