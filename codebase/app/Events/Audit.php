<?php

namespace App\Events;

use Smorken\Lookup\Constants\LookupType;

class Audit
{
    public function __construct(
        public string $lookupId,
        public LookupType $lookupType,
        public int $userId
    ) {
    }
}
