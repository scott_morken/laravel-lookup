<?php $filtered = 'border border-success'; ?>
<div class="card my-2">
    <div class="card-body">
        <form method="get" class="row g-3 align-items-center">
            <div class="col-md">
                <label for="user_id" class="visually-hidden">User ID</label>
                <input type="text" class="form-control {{ $filter->r_userId ? $filtered : '' }}" name="user_id"
                       value="{{ $filter->f_userId }}"
                       placeholder="User ID">
            </div>
            <div class="col-md">
                <label for="lookup_id" class="visually-hidden">Lookup ID</label>
                <input type="text" class="form-control {{ $filter->r_userId ? $filtered : '' }}" name="lookup_id"
                       value="{{ $filter->f_lookupId }}"
                       placeholder="Lookup ID">
            </div>
            <div class="col-md">
                <label for="start" class="visually-hidden">Start Date</label>
                <input type="text" class="form-control {{ $filter->r_userId ? $filtered : '' }}" name="start"
                       value="{{ $filter->f_start }}"
                       placeholder="after YYYY-mm-dd">
            </div>
            <div class="col-md">
                <label for="end" class="visually-hidden">End Date</label>
                <input type="text" class="form-control {{ $filter->r_userId ? $filtered : '' }}" name="end"
                       value="{{ $filter->f_end }}"
                       placeholder="before YYYY-mm-dd">
            </div>
            <div class="col-md">
                <button type="submit" class="btn btn-primary">Filter</button>
                <a href="{{ action([$controller, 'index']) }}" class="btn btn-outline-warning" title="Reset">Reset</a>
            </div>
        </form>
    </div>
</div>
