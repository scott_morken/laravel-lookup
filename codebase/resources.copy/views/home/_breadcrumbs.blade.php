<?php
/**
 * @var \App\Contracts\Tools\History $history
 */
?>
@if ($history->getOrdered())
    <nav aria-label="breadbrumb">
        <ol class="breadcrumb">
            @foreach ($history->getOrdered() as $label => $url)
                <li class="breadcrumb-item">
                    <a href="{{ $url }}" title="{{ $label }}" class="text-underline-hover">{{ $label }}</a>
                </li>
            @endforeach
        </ol>
    </nav>
@endif
