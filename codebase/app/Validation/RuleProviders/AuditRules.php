<?php

declare(strict_types=1);

namespace App\Validation\RuleProviders;

class AuditRules
{
    public static function rules(array $overrides = []): array
    {
        return [
            'lookup_id' => 'required|max:32',
            'lookup_type' => 'required|in:person,klass|max:16',
            'user_id' => 'required|int',
            ...$overrides,
        ];
    }
}
