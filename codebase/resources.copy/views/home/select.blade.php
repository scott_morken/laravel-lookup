<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 4/2/18
 * Time: 12:59 PM
 * @var \App\Models\VO\Result $result
 */
?>
@extends('layouts.app')
@section('content')
    @include('home._breadcrumbs')
    <div>
        <span class="text-muted">Max 50 results.</span>
        @if ($result->type === \Smorken\Lookup\Constants\LookupType::PERSON)
            @include('home.sub._toggle_mask')
        @endif
    </div>
    <div class="list-group">
        @if (count($result->result))
            @foreach($result->result as $model)
                <a href="{{ action([$controller, 'view'], ['type' => $result->type, 'id' => $model->getModelId()]) }}"
                   title="Select {{ $result->type }} #{{ $model->getModelId() }}"
                   class="list-group-item list-group-item-action select-result" dusk="select-result">
                    @if ($result->type === \Smorken\Lookup\Constants\LookupType::PERSON)
                        @include('home.sub._select_person')
                    @else
                        @include('home.sub._select_klass')
                    @endif
                </a>
            @endforeach
        @else
            <div class="text-muted">No results found.</div>
        @endif
    </div>
    @include('home.sub._disclaimer')
@stop
@push('add_to_end')
    <script src="{{ asset('js/limited/home.mask.js') }}"></script>
@endpush
