<?php

return [
    'autoload' => env('SERVICES_AUTOLOAD', false),
    'path' => env('SERVICES_INVOKABLE_PATH', 'Providers/Services'),
    'invokables' => [
        \App\Providers\Services\Admin\AuditServices::class,
        \App\Providers\Services\HomeServices::class,
    ],
];
