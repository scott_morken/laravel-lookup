<?php

namespace App\Storage\Eloquent;

use App\Validation\RuleProviders\AuditRules;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Lookup\Constants\LookupType;

class Audit extends Base implements \App\Contracts\Storage\Audit
{
    public function store(
        string $lookupId,
        LookupType $lookupType,
        int $userId
    ): bool {
        $attrs = [
            'lookup_id' => $lookupId,
            'lookup_type' => $lookupType,
            'user_id' => $userId,
        ];

        return (bool) $this->create($attrs);
    }

    public function validationRules(array $override = []): array
    {
        return AuditRules::rules($override);
    }

    protected function filterEnd(Builder $query, $value): Builder
    {
        if (strlen((string) $value)) {
            $query = $query->createdBefore($value);
        }

        return $query;
    }

    protected function filterLookupId(Builder $query, $value): Builder
    {
        if (strlen((string) $value)) {
            $query = $query->lookupIdIs($value);
        }

        return $query;
    }

    protected function filterLookupType(Builder $query, $value): Builder
    {
        if (strlen((string) $value)) {
            $value = LookupType::tryFrom($value);
            if ($value) {
                $query = $query->lookupTypeIs($value);
            }
        }

        return $query;
    }

    protected function filterStart(Builder $query, $value): Builder
    {
        if (strlen((string) $value)) {
            $query = $query->createdAfter($value);
        }

        return $query;
    }

    protected function filterUserId(Builder $query, $value): Builder
    {
        if (strlen((string) $value)) {
            $query = $query->userIdIs($value);
        }

        return $query;
    }

    protected function getFilterMethods(): array
    {
        return [
            'f_userId' => 'filterUserId',
            'f_start' => 'filterStart',
            'f_end' => 'filterEnd',
            'f_lookupType' => 'filterLookupType',
            'f_lookupId' => 'filterLookupId',
        ];
    }
}
