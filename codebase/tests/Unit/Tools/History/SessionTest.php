<?php

namespace Tests\App\Unit\Tools\History;

use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Contracts\Session\Session;
use Mockery as m;
use PHPUnit\Framework\TestCase;

class SessionTest extends TestCase
{
    public function testInitializeWithBackendSet(): void
    {
        [$sut, $session] = $this->getSut();
        $session->shouldReceive('exists')->once()->with('lookup_history')->andReturn(true);
        $session->shouldReceive('get')->once()->with('lookup_history')->andReturn(['search' => 'foo']);
        $expected = ['home' => 'http://home', 'search' => 'foo', 'trail' => []];
        $this->assertEquals($expected, $sut->get());
    }

    public function testInitializeWithEmptyBackendSetsHome()
    {
        [$sut, $session] = $this->getSut();
        $session->shouldReceive('exists')->once()->with('lookup_history')->andReturn(false);
        $expected = ['home' => 'http://home', 'search' => null, 'trail' => []];
        $this->assertEquals($expected, $sut->get());
    }

    public function testSearchResetsTrail(): void
    {
        [$sut, $session] = $this->getSut();
        $session->shouldReceive('exists')->once()->with('lookup_history')->andReturn(false);
        $session->shouldReceive('put')->with('lookup_history', m::type('array'));
        $sut->set('trail', 't1');
        $sut->set('trail', 't2');
        $sut->set('trail', 't3');
        $this->assertCount(3, $sut->getByKey('trail'));
        $sut->set('search', 'foo');
        $this->assertCount(0, $sut->getByKey('trail'));
    }

    public function testTrailMaxOfTrailCount(): void
    {
        [$sut, $session] = $this->getSut();
        $session->shouldReceive('exists')->once()->with('lookup_history')->andReturn(false);
        $session->shouldReceive('put')->with('lookup_history', m::type('array'));
        $sut->set('trail', 't1');
        $sut->set('trail', 't2');
        $sut->set('trail', 't3');
        $this->assertCount(3, $sut->getByKey('trail'));
        $sut->set('trail', 't4');
        $this->assertCount(3, $sut->getByKey('trail'));
        $expected = [
            ['name' => null, 'value' => 't2'], ['name' => null, 'value' => 't3'], ['name' => null, 'value' => 't4'],
        ];
        $this->assertEquals($expected, $sut->getByKey('trail'));
    }

    public function testTrailMaxOfTrailCountMaxCountOfOne(): void
    {
        [$sut, $session] = $this->getSut('http://home', 1);
        $session->shouldReceive('exists')->once()->with('lookup_history')->andReturn(false);
        $session->shouldReceive('put')->with('lookup_history', m::type('array'));
        $sut->set('trail', 't1');
        $sut->set('trail', 't2');
        $sut->set('trail', 't3');
        $this->assertCount(1, $sut->getByKey('trail'));
        $sut->set('trail', 't4');
        $this->assertCount(1, $sut->getByKey('trail'));
        $expected = [['name' => null, 'value' => 't4']];
        $this->assertEquals($expected, $sut->getByKey('trail'));
    }

    public function testTrailWithNames(): void
    {
        [$sut, $session] = $this->getSut();
        $session->shouldReceive('exists')->once()->with('lookup_history')->andReturn(false);
        $session->shouldReceive('put')->with('lookup_history', m::type('array'));
        $sut->set('trail', 't1');
        $sut->set('trail', 't2', 'foo');
        $sut->set('trail', 't3');
        $this->assertCount(3, $sut->getByKey('trail'));
        $sut->set('trail', 't4');
        $this->assertCount(3, $sut->getByKey('trail'));
        $expected = [
            ['name' => 'foo', 'value' => 't2'], ['name' => null, 'value' => 't3'], ['name' => null, 'value' => 't4'],
        ];
        $this->assertEquals($expected, $sut->getByKey('trail'));
    }

    protected function getSut(string $home = 'http://home', int $trail_count = 3, string $key = 'lookup_history'): array
    {
        $b = m::mock(Session::class);
        $url = m::mock(UrlGenerator::class);
        $sut = new \App\Tools\History\Session($home, $b, $url, $trail_count, $key);

        return [$sut, $b];
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
