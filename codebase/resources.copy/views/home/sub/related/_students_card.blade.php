<?php
/**
 * @var \Smorken\Lookup\Contracts\Models\Klass $model
 */
$enrollmentsHelper = new \App\View\Helpers\ClassEnrollmentsHelper($model);
$enrollments = $enrollmentsHelper->enrolled();
?>
<h4 class="card-title">Students</h4>
<div class="card-text students">
    @if (count($enrollments))
        <div class="list-group list-group-flush enrollment">
            @foreach ($enrollments as $enrollment)
                <a href="{{ action([$controller, 'view'], ['type' => \Smorken\Lookup\Constants\LookupType::PERSON, 'id' => $enrollment->studentId]) }}"
                   class="list-group-item list-group-item-action student"
                   title="View student #{{ $enrollment->studentId }}">
                    @include('home.sub.detail._student')
                </a>
            @endforeach
        </div>
    @else
        No enrollments found.
    @endif
</div>
