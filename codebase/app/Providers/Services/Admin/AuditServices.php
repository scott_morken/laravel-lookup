<?php

namespace App\Providers\Services\Admin;

use App\Contracts\Services\Audits\Admin\IndexService;
use App\Contracts\Storage\Audit;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Invokables\Invokable;

class AuditServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(IndexService::class, fn ($app) => new \App\Services\Audits\Admin\IndexService(
            $app[Audit::class],
            [
                FilterService::class => new \App\Services\Audits\Admin\FilterService(),
            ]
        ));
    }
}
