<?php

namespace App\Contracts\Services\Home;

use Smorken\Service\Contracts\Services\VO\VOResult;

/**
 * @property array $filter
 */
interface ArrayFilterResult extends VOResult
{
    public function valueOf(string $key): mixed;

    public function get(string $key, mixed $default = null): mixed;

    public function set(string $key, mixed $value): void;
}
