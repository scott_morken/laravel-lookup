<?php

namespace App\Contracts\Services\Home;

use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\BaseService;

interface ArrayFilterService extends BaseService
{
    public function getFromRequest(Request $request): ArrayFilterResult;
}
