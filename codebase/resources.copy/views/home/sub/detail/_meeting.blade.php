<?php
/**
 * @var \Smorken\Sis\Contracts\Base\Klass\Meeting $meeting
 * @var \Smorken\Sis\Contracts\Base\Klass\KlassInstructor $instructor
 */
?>
<div class="row">
    <div class="col-md-3 meeting-facility">{{ $meeting->room }}</div>
    <div class="col-md-3">{{ $meeting->daysVO->daysToString() }}</div>
    @if ($meeting->startTime)
        <div class="col-md meeting-times">
            {{ $meeting->startTime->format('g:i A') }} - {{ $meeting->endTime?->format('g:i A') ?? '--' }}
        </div>
    @endif
</div>
@if ($meeting->startDate || $meeting->endDate)
    <div class="meeting-dates">
            {{ $meeting->startDate?->toFormattedDateString() }}
            - {{ $meeting->endDate?->toFormattedDateString() }}
    </div>
@endif
<div class="row instructors">
    @foreach ($meeting->classInstructors as $instructor)
        <div class="col instructor">
            <a href="{{ action([$controller, 'view'], ['type' => \Smorken\Lookup\Constants\LookupType::PERSON, 'id' => $instructor->instructorId]) }}"
               title="View instructor #{{ $instructor->instructorId }}" class=" text-underline-hover">
                {{ $instructor->shortName }}
                [{{ $instructor->roleCode }}/{{ $instructor->workloadHours }}/{{ $instructor->assignmentTypeCode }}]
            </a>
        </div>
    @endforeach
</div>

