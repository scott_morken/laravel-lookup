<?php

namespace App\Contracts\Tools;

use Illuminate\Contracts\Routing\UrlGenerator;

interface History
{
    public function getUrlGenerator(): UrlGenerator;

    public function get(): array;

    public function getBackend(): mixed;

    public function getByKey(string $key): mixed;

    public function getOrdered(): array;

    public function set(string $key, mixed $value, ?string $name = null): void;

    public function setBackend(mixed $backend): void;
}
