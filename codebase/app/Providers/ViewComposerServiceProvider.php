<?php

namespace App\Providers;

use App\View\Composers\Terms;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    protected array $viewComposers = [
        Terms::class => [
            'home._search_form',
        ],
    ];

    public function boot(): void
    {
        foreach ($this->viewComposers as $className => $views) {
            $this->bootViewComposers($className, $views);
        }
    }

    protected function bootViewComposers(string $className, array $views): void
    {
        View::composer($views, $className);
    }
}
