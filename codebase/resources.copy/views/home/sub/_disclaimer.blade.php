<div class="text-muted small text-md-center mt-3">
    The information displayed here is personally identifiable information (PII) and may include information
    protected under the Family Educational Rights and Privacy Act (FERPA).
</div>
