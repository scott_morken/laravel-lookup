<?php

return [
    'concrete' => [
        \App\Storage\Eloquent\Audit::class => [
            'model' => [
                'impl' => \App\Models\Eloquent\Audit::class,
            ],
        ],
    ],
    'contract' => [
        \App\Contracts\Storage\Audit::class => \App\Storage\Eloquent\Audit::class,
    ],
];
