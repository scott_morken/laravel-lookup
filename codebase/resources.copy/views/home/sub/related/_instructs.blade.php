<?php
/**
 * @var \Smorken\Lookup\Contracts\Models\Person $model
 */
$instructsHelper = new \App\View\Helpers\InstructsHelper($model);
$instructs = $instructsHelper->instructs();
?>
@if ($model->isFaculty())
    <div class="float-end">
        <a class="text-decoration-none link-dark" data-bs-toggle="collapse" data-bs-target=".meeting-pattern" role="button"
           aria-expanded="false">
            <i class="bi bi-card-list"><span class="visually-hidden">Expand/collapse all meetings</span></i>
        </a>
    </div>
    <h4 class="card-title">Instructs</h4>
    <div class="card-text instructs-klasses">
        @if (count($instructs))
            <div class="instructs-klasses list-group list-group-flush">
                @foreach ($instructs as $klass)
                    <div class="instructs-klass list-group-item list-group-item-action">
                        <a href="{{ action([$controller, 'view'], ['type' => \Smorken\Lookup\Constants\LookupType::KLASS, 'id' => $klass->getModelId()]) }}"
                           title="Select class {{ $klass->className }}"
                           class="text-decoration-none link-dark">
                            @include('home.sub.detail._klass')
                        </a>
                        <a class="text-decoration-none link-dark" data-bs-toggle="collapse"
                           data-bs-target="#meetings-{{ $klass->getModelId() }}"
                           role="button"
                           aria-expanded="false" aria-controls="meetings-{{ $klass->getModelId() }}">
                            <i class="bi bi-card-list">
                                <span class="visually-hidden">Expand/collapse meetings for class {{ $klass->getModelId() }}</span>
                            </i>
                        </a>
                        <div class="collapse meeting-pattern" id="meetings-{{ $klass->getModelId() }}">
                            @include('home.sub.related._meetings')
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            Not instructing any classes.
        @endif
    </div>
@endif
