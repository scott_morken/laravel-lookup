<?php
/**
 * @var \Smorken\Lookup\Contracts\Models\Person $model
 * @var \Smorken\Sis\Contracts\Base\Student\Enrollment $enrollment
 */
$enrollmentsHelper = new \App\View\Helpers\StudentEnrollmentsHelper($model);
$enrollments = $enrollmentsHelper->enrolled();
?>
<h4 class="card-title">Enrollments</h4>
<div class="card-text attends-klasses">
    @if (count($enrollments))
        <div class="list-group list-group-flush">
            @foreach ($enrollments as $enrollment)
                <?php
                $helper = new \App\View\Helpers\EnrollmentHelper($enrollment);
                $klass = $enrollment->class;
                ?>
                @if ($klass)
                    <a href="{{ action([$controller, 'view'], ['type' => \Smorken\Lookup\Constants\LookupType::KLASS, 'id' => $klass->getModelId()]) }}"
                       title="Select class {{ $klass->className }}"
                       class="list-group-item list-group-item-action attends-klass">
                        @include('home.sub.detail._klass')
                        <div class="{{ $helper->classes() }}">{{ $helper->text() }}</div>
                    </a>
                @endif
            @endforeach
        </div>
    @else
        Not enrolled in any classes.
    @endif
</div>
