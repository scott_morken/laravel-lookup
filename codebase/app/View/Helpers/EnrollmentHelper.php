<?php

namespace App\View\Helpers;

use Smorken\Lookup\Contracts\Models\Enrollment;
use Smorken\Sis\Enums\Student\EnrollmentStatuses;

class EnrollmentHelper
{
    public function __construct(protected Enrollment $enrollment)
    {
    }

    public function studentName(): string
    {
        if ($this->enrollment->student) {
            return $this->enrollment->student->fullName();
        }
        return $this->enrollment->studentId;
    }

    public function classes(): string
    {
        $base = 'enrollment-status-'.strtolower($this->enrollment->statusCode ?? 'unk');
        if (in_array($this->enrollment->statusCode,
            [EnrollmentStatuses::ENROLLED, EnrollmentStatuses::ENROLLED_WAITLIST])) {
            return $base.' text-success';
        }

        return $base.' text-danger';
    }

    public function text(): string
    {
        return $this->enrollment->statusDescription;
    }
}
