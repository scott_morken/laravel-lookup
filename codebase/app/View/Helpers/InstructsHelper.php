<?php

namespace App\View\Helpers;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Smorken\Lookup\Contracts\Models\Person;
use Smorken\Sis\Contracts\Base\Klass\Klass;
use Smorken\Sis\Enums\Relations;

class InstructsHelper
{
    public function __construct(public Person $person)
    {
    }

    public function getCurrentAndUpcoming(): Collection
    {
        $classes = new Collection();
        $instructsKlasses = $this->person->instructsClasses()
            ->with($this->getRelations())
            ->whereHas(Relations::KLASS, function (Builder $query) {
                $query->notCancelled()
                    ->endAfter(Carbon::now()->subWeek());
            })
            ->get();
        /** @var \Smorken\Lookup\Contracts\Models\KlassInstructor $instructsKlass */
        foreach ($instructsKlasses as $instructsKlass) {
            $klass = $instructsKlass->class;
            if ($klass) {
                $classes->push($klass);
            }
        }

        return $classes->sortBy(fn (Klass $class) => $class->termId.$class->startDate);
    }

    public function instructs(): Collection
    {
        return $this->getCurrentAndUpcoming();
    }

    protected function getRelations(): array
    {
        return [
            Relations::KLASS,
            $this->createRelation([Relations::KLASS, Relations::TERM]),
            $this->createRelation([Relations::KLASS, Relations::MEETINGS]),
            $this->createRelation([Relations::KLASS, Relations::MEETINGS, Relations::CLASS_INSTRUCTORS]),
        ];
    }

    protected function createRelation(array $relations): string
    {
        return implode('.', $relations);
    }
}
