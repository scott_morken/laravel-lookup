<?php
/**
 * @var \Smorken\Lookup\Contracts\Models\Klass $model
 */
?>
<h4 class="card-title">Meetings</h4>
<div class="card-text meetings">
    @if ($model->meetings && count($model->meetings))
        @include('home.sub.related._meetings', ['klass' => $model])
    @else
        No meeting patterns found.
    @endif
</div>
