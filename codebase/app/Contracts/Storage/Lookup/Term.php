<?php

namespace App\Contracts\Storage\Lookup;

use Illuminate\Support\Collection;

interface Term extends \Smorken\Lookup\Contracts\Storage\Term
{
    public function currentAndUpcoming(): Collection;
}
