<?php

namespace App\View\Helpers;

use Illuminate\Support\Collection;
use Smorken\Lookup\Contracts\Models\Enrollment;
use Smorken\Lookup\Contracts\Models\Klass;
use Smorken\Sis\Enums\Relations;
use Smorken\Sis\Enums\Student\EnrollmentStatuses;

class ClassEnrollmentsHelper
{
    public function __construct(public Klass $klass)
    {
    }

    public function enrolled(): Collection
    {
        $this->klass->load($this->getClassRelations());

        return $this->orderEnrollments($this->klass->enrollments);
    }

    protected function createRelation(array $relations): string
    {
        return implode('.', $relations);
    }

    protected function getClassRelations(): array
    {
        return [
            Relations::ENROLLMENTS,
            $this->createRelation([Relations::ENROLLMENTS, Relations::STUDENT]),
            Relations::MEETINGS,
            $this->createRelation([Relations::MEETINGS, Relations::CLASS_INSTRUCTORS]),
        ];
    }

    protected function orderEnrollments(Collection $enrollments): Collection
    {
        return $enrollments->sortBy(function (Enrollment $enrollment) {
            $key = [
                in_array($enrollment->statusCode,
                    [EnrollmentStatuses::ENROLLED, EnrollmentStatuses::ENROLLED_WAITLIST]) ? 'AAA' : 'ZZZ',
                $enrollment->student?->lastName,
                $enrollment->student?->firstName,
            ];

            return implode('', $key);
        });
    }
}
