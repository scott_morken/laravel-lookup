<?php

namespace App\View\Helpers;

use App\Enums\YesNo;

class YesNoHelper
{
    public YesNo $yesNo;

    public function __construct(YesNo|bool $yesNo)
    {
        if (is_bool($yesNo)) {
            $yesNo = $yesNo ? YesNo::YES : YesNo::NO;
        }
        $this->yesNo = $yesNo;
    }

    public function classes(): string
    {
        return match ($this->yesNo) {
            YesNo::YES => 'text-success bi bi-check',
            YesNo::NO => 'text-danger bi bi-x',
        };
    }

    public function text(): string
    {
        return $this->yesNo->text();
    }
}
