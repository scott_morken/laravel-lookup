<?php

namespace App\Services\Audits\Admin;

use Smorken\Service\Services\PaginateByFilterService;

class IndexService extends PaginateByFilterService implements \App\Contracts\Services\Audits\Admin\IndexService
{
}
