<?php

namespace App\Services\Home;

use App\Contracts\Services\Home\ArrayFilterService;
use App\Contracts\Services\Home\LookupResult;
use App\Contracts\Tools\History;
use App\Events\Audit;
use App\Http\Controllers\HomeController;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Smorken\Lookup\Constants\LookupType;
use Smorken\Lookup\Contracts\Models\Klass;
use Smorken\Lookup\Contracts\Models\Person;
use Smorken\Lookup\Factory;
use Smorken\Service\Services\BaseService;
use Smorken\Sis\Enums\Relations;

class LookupService extends BaseService implements \App\Contracts\Services\Home\LookupService
{
    public function __construct(
        protected Factory $factory,
        protected History $history,
        protected Dispatcher $dispatcher,
        array $services = []
    ) {
        parent::__construct($services);
    }

    public function getArrayFilterService(): ArrayFilterService
    {
        return $this->getService(ArrayFilterService::class);
    }

    public function getEventDispatcher(): Dispatcher
    {
        return $this->dispatcher;
    }

    public function getHistory(): History
    {
        return $this->history;
    }

    public function getLookup(): Factory
    {
        return $this->factory;
    }

    public function lookup(Request $request): LookupResult
    {
        $arrayFilterResult = $this->getFilter($request);
        $result = new \App\Services\Home\LookupResult($arrayFilterResult, $this->getHistory());
        $request = $this->addClassNameOrNumberToRequest($request);
        if ($this->getLookup()->willSearch($request)) {
            $this->getHistory()
                ->set('search', $this->getHistory()->getUrlGenerator()->route('home', $arrayFilterResult->filter));
            $this->handleLookup($request, $result);
        }

        return $result;
    }

    public function view(Request $request, LookupType|string $type, string $id): LookupResult
    {
        if (is_string($type)) {
            $type = LookupType::from($type);
        }
        $model = $this->getLookup()->create($type)->find($id);
        if (! $model) {
            throw new ModelNotFoundException("Resource $type->value #$id not found.");
        }
        $this->eagerLoads($model);
        $arrayFilterResult = $this->getFilter($request);
        $result = new \App\Services\Home\LookupResult($arrayFilterResult, $this->getHistory());
        $this->forView($request, $result, $type, $id, $model);

        return $result;
    }

    protected function addClassNameOrNumberToRequest(Request $request): Request
    {
        $nameOrNumber = $request->input('filter.class.norn');
        if ($nameOrNumber) {
            if (preg_match('/[0-9]{5}/', (string) $nameOrNumber)) {
                return $request->merge([
                    'filter' => [
                        'class' => [
                            ...$request->input('filter.class') ?? [], 'classNumber' => $nameOrNumber,
                        ],
                    ],
                ]);
            }

            return $request->merge([
                'filter' => [
                    'class' => [
                        ...$request->input('filter.class') ?? [], 'className' => $nameOrNumber,
                    ],
                ],
            ]);
        }

        return $request;
    }

    protected function createRelation(array $relations): string
    {
        return implode('.', $relations);
    }

    protected function eagerLoads(\Smorken\Lookup\Contracts\Models\Lookup $model): void
    {
        if ($model instanceof Person) {
            // do nothing
        }
        if ($model instanceof Klass) {
            $model->load($this->getClassRelations());
        }
    }

    protected function forView(
        Request $request,
        LookupResult $result,
        LookupType $type,
        string $id,
        \Smorken\Lookup\Contracts\Models\Lookup $model
    ): void {
        $this->getHistory()
            ->set(
                'trail',
                $this->getHistory()->getUrlGenerator()->action(
                    [HomeController::class, 'view'],
                    ['type' => $type->value, 'id' => $id]
                ),
                $model->getShortName()
            );
        $result->viewName = 'view';
        $result->addWiths([
            'model' => $model,
            'type' => $type,
        ]);
        $this->getEventDispatcher()->dispatch(new Audit($id, $type, $this->getUserId($request)));
    }

    protected function getClassRelations(): array
    {
        return [
            Relations::ENROLLMENTS,
            $this->createRelation([Relations::ENROLLMENTS, Relations::STUDENT]),
            Relations::MEETINGS,
            $this->createRelation([Relations::MEETINGS, Relations::CLASS_INSTRUCTORS]),
        ];
    }

    protected function getFilter(Request $request): \App\Contracts\Services\Home\ArrayFilterResult
    {
        return $this->getArrayFilterService()->getFromRequest($request);
    }

    protected function getUserId(Request $request): int
    {
        return $request->user()->getAuthIdentifier();
    }

    protected function handleLookup(Request $request, LookupResult $result): void
    {
        $model = $this->getLookup()->fromRequest($request);
        if ($model->isCollection()) {
            if ($model->result->isEmpty()) {
                $result->message = 'Nothing matched your search request.';

                return;
            }
            if ($model->result->count() > 1) {
                $result->viewName = 'select';
                $result->addWith('result', $model);

                return;
            }
            if ($model->result->count() === 1) {
                $model->result = $model->result->first();
            }
        }
        if ($model->hasResult()) {
            $this->forView($request, $result, $model->type, $model->result->getModelId(),
                $model->result);
        }
    }
}
