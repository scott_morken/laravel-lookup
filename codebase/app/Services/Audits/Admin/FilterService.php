<?php

namespace App\Services\Audits\Admin;

use Illuminate\Http\Request;
use Smorken\Support\Contracts\Filter;

class FilterService extends \Smorken\Service\Services\FilterService
{
    protected function createFilterFromRequest(Request $request): Filter
    {
        return new \Smorken\Support\Filter([
            'f_userId' => $request->input('f_userId'),
            'f_start' => $request->input('f_start'),
            'f_end' => $request->input('f_end'),
            'f_lookupId' => $request->input('f_lookupId'),
            'f_lookupType' => $request->input('f_lookupType'),
        ]);
    }
}
