<?php
/**
 * @var \App\Contracts\Services\Home\ArrayFilterResult $filterResult
 * @var \Illuminate\Support\Collection $terms
 * @var \Smorken\Lookup\Contracts\Models\Term $term
 * @var \Smorken\Lookup\Contracts\Models\Term $activeTerm
 */
$activeTermId = $filterResult->valueOf('filter.class.termId') ?? $activeTerm->id;
?>
<form method="get" id="search-form">
    <div class="row mb-2">
        <div class="col-md">
            <div class="card h-100">
                <div class="card-header">
                    <h5 class="card-title">Person Search</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md mb-1">
                            <label for="person-last" class="visually-hidden">Last Name</label>
                            <input type="text" class="form-control" id="person-last" name="filter[person][lastName]"
                                   placeholder="Last name..." dusk="search-last-name"
                                   value="{{ $filterResult->valueOf('filter.person.lastName') }}">
                        </div>
                        <div class="col-md mb-1">
                            <label for="person-first" class="visually-hidden">First Name</label>
                            <input type="text" class="form-control" id="person-first" name="filter[person][firstName]"
                                   placeholder="First name..." dusk="search-first-name"
                                   value="{{ $filterResult->valueOf('filter.person.firstName') }}">
                        </div>
                    </div>
                    <div class="text-center align-middle mb-1">OR</div>
                    <div class="col-md mb-1">
                        <label for="person-id" class="visually-hidden">MEID or Student ID</label>
                        <input type="text" class="form-control" id="person-id" name="filter[person][id]"
                               placeholder="MEID or Student ID" dusk="search-person-id"
                               value="{{ $filterResult->valueOf('filter.person.id') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md">
            <div class="card h-100">
                <div class="card-header">
                    <h5 class="card-title">Class Search</h5>
                </div>
                <div class="card-body">
                    <div class="mb-2">
                        <label for="class-term" class="visually-hidden">Term</label>
                        <select name="filter[class][termId]" id="class-term" class="form-select" dusk="search-class-term">
                            @foreach ($terms as $term)
                                <option value="{{ $term->id }}" {{ $term->id === $activeTermId ? 'selected' : null }}>
                                    {{ $term->description }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-1">
                        <label for="class-norn" class="visually-hidden">Subject and Catalog Number</label>
                        <input type="text" class="form-control" id="class-norn" name="filter[class][norn]"
                               placeholder="Class Number or ABC101" dusk="search-class-norn"
                               value="{{ $filterResult->valueOf('filter.class.norn') }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="button-group">
        <button type="submit" class="btn btn-primary" name="search" dusk="search-button">Search</button>
        <a href="{{ route('home') }}" title="Reset Form" class="btn btn-outline-warning" dusk="search-reset">Reset</a>
    </div>
</form>

