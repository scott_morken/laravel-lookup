<div>
    <small class="text-muted small">The blur (<span class="blur can-blur">blur</span>) feature
        is not a security feature. It exists to help prevent casual shoulder surfing.
    </small>
</div>
