<span id="toggle-mask" class="checkable">
    <i class="bi bi-eye-slash-fill text-danger masked"></i> <span
            class="visually-hidden">Toggle masked visibility</span>
</span>
