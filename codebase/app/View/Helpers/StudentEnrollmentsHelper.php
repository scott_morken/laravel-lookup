<?php

namespace App\View\Helpers;

use Illuminate\Support\Collection;
use Smorken\Lookup\Contracts\Models\Enrollment;
use Smorken\Lookup\Contracts\Models\Person;
use Smorken\Sis\Enums\Relations;

class StudentEnrollmentsHelper
{
    public function __construct(public Person $person)
    {
    }

    public function enrolled(): Collection
    {
        return $this->sortEnrollments($this->getCurrentAndUpcomingEnrollments());
    }

    protected function getCurrentAndUpcomingEnrollments(): Collection
    {
        return $this->person->enrollments()
            ->with([
                Relations::KLASS,
                Relations::KLASS.'.'.Relations::TERM,
            ])
            ->currentAndUpcomingByTerm()
            ->activeByClass()
            ->get();
    }

    protected function sortEnrollments(Collection $enrollments): Collection
    {
        return $enrollments->sortBy(fn (Enrollment $enrollment) => $enrollment->termId.$enrollment->class?->startDate);
    }
}
