<?php
/**
 * @var \Smorken\Lookup\Contracts\Models\Klass $klass
 * @var \Smorken\Sis\Contracts\Base\Klass\Meeting $meeting
 */
?>
@foreach ($klass->meetings as $meeting)
    @include('home.sub.detail._meeting')
@endforeach
