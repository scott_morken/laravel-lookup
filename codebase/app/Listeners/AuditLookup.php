<?php

namespace App\Listeners;

use App\Contracts\Storage\Audit;
use Illuminate\Contracts\Debug\ExceptionHandler;

class AuditLookup
{
    public function __construct(
        protected Audit $audit,
        protected ExceptionHandler $exceptionHandler
    ) {
    }

    public function handle(\App\Events\Audit $audit): void
    {
        try {
            $this->audit->store($audit->lookupId, $audit->lookupType, $audit->userId);
        } catch (\Throwable $e) {
            $this->exceptionHandler->report($e);
        }
    }
}
